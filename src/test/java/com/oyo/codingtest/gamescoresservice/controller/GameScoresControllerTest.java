package com.oyo.codingtest.gamescoresservice.controller;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.net.URISyntaxException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Collections;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.platform.runner.JUnitPlatform;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.jayway.jsonpath.JsonPath;
import com.oyo.codingtest.gamescoresservice.model.ScoreResponse;
import com.oyo.codingtest.gamescoresservice.repository.GameScoresRespository;
import com.oyo.codingtest.gamescoresservice.service.GameScoresService;
import com.oyo.codingtest.gamescoresservice.util.Constants;
import com.oyo.codingtest.gamescoresservice.util.HttpUtil;
import com.oyo.codingtest.gamescoresservice.utils.TestUtils;

@RunWith(JUnitPlatform.class)
public class GameScoresControllerTest {

	@InjectMocks
	private GameScoresController gameScoresController;

	@Spy
	private GameScoresControllerAdvice gameScoresControllerAdvice;
	
	@Mock
	private GameScoresService gameScoresService;
	@Mock
	private GameScoresRespository gameScoresRespository;
	
	@Spy
	private HttpHeaders headers;
	private MockMvc mockMvc;
	
	@Spy
	private Pageable pageable;
	
	@BeforeEach
	public void init() throws IOException, URISyntaxException {

		MockitoAnnotations.initMocks(this);
		mockMvc = MockMvcBuilders.standaloneSetup(gameScoresController).setControllerAdvice(gameScoresControllerAdvice).build();

		headers.add("content_type", MediaType.APPLICATION_JSON_VALUE);
	}

	@Test
	public void testSaveScoreOK() throws Exception {
		when(gameScoresService.save(TestUtils.getScoreTblObj())).thenReturn(HttpUtil.buildResponseNoPayload());
		RequestBuilder requestBuilder = MockMvcRequestBuilders.post("/createScore")
										.headers(headers)
										.contentType(MediaType.APPLICATION_JSON_VALUE)
										.accept(MediaType.APPLICATION_JSON)
										.content(TestUtils.getScoreTblStrObj());

		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
		MockHttpServletResponse returnedResponse = result.getResponse();
		assertEquals(HttpStatus.OK.value(), returnedResponse.getStatus());
		
	}
	
	@Test
	public void testGetScoreByPlayerOK() throws Exception {
		when(gameScoresService.getScoreById(1)).thenReturn(HttpUtil.buildResponseGetScore(Optional.of(TestUtils.getScoreTblObjWithId())));
		RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/getScore")
										.headers(headers)
										.contentType(MediaType.APPLICATION_JSON_VALUE)
										.accept(MediaType.APPLICATION_JSON)
										.param("id", "1");
		
		  MvcResult result = mockMvc.perform(requestBuilder).andReturn();
		  MockHttpServletResponse returnedResponse = result.getResponse();
		  assertEquals(HttpStatus.OK.value(), returnedResponse.getStatus());
		  assertNotNull(JsonPath.read(returnedResponse.getContentAsString(), "$.payload[0]"));
	}
	
	@Test
	public void testDeleteScoreByIdOK() throws Exception {
		when(gameScoresService.deleteScoreById(1)).thenReturn(HttpUtil.buildResponseNoPayload());
		RequestBuilder requestBuilder = MockMvcRequestBuilders.delete("/deleteScore")
										.headers(headers)
										.contentType(MediaType.APPLICATION_JSON_VALUE)
										.accept(MediaType.APPLICATION_JSON)
										.param("id", "1");

		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
		MockHttpServletResponse returnedResponse = result.getResponse();
		assertEquals(HttpStatus.OK.value(), returnedResponse.getStatus());
		
	}
	
	@Test
	public void testGetPlayerHistoryOK() throws Exception {
		when(gameScoresService.getPlayerHistory("A")).thenReturn(HttpUtil.buildResponseGetHistoryScore(TestUtils.getPlayerHistoryObj()));
		RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/getPlayerHistory")
										.headers(headers)
										.contentType(MediaType.APPLICATION_JSON_VALUE)
										.accept(MediaType.APPLICATION_JSON)
										.param("player", "A");

		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
		MockHttpServletResponse returnedResponse = result.getResponse();
		assertEquals(HttpStatus.OK.value(), returnedResponse.getStatus());
		assertEquals(Integer.valueOf(100), JsonPath.read(returnedResponse.getContentAsString(), "$.payload.averageScore"));
		assertEquals(Integer.valueOf(100), JsonPath.read(returnedResponse.getContentAsString(), "$.payload.topTimeScore.score"));
		assertEquals(Integer.valueOf(100), JsonPath.read(returnedResponse.getContentAsString(), "$.payload.leastTimeScore.score"));
		assertEquals(Integer.valueOf(100), JsonPath.read(returnedResponse.getContentAsString(), "$.payload.allScores[0].score"));

	}
	
	@Test
	public void testGetScoresListOK() throws Exception {
		
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern(Constants.DATE_PATTERN);
		String startDate= "2020-01-01 00:00:00";
		String endDate= "2021-01-31 00:00:00";
		  
		when(gameScoresService.getScoresList(LocalDateTime.parse(startDate, formatter),
											LocalDateTime.parse(endDate, formatter),"A",pageable))
			.thenReturn(HttpUtil.buildResponseGetScoreList(Collections.singletonList(TestUtils.getScoreTblObjWithId())));
		
		ResponseEntity<ScoreResponse> response = gameScoresController.getScoresList(LocalDateTime.parse(startDate, formatter),
				LocalDateTime.parse(endDate, formatter),"A",pageable);
        
		assertEquals(HttpStatus.OK.value(), response.getStatusCodeValue());
		
		
	}
	
}
