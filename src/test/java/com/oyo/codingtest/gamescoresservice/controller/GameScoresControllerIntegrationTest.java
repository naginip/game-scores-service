package com.oyo.codingtest.gamescoresservice.controller;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.stream.Stream;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.platform.runner.JUnitPlatform;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.jayway.jsonpath.JsonPath;
import com.oyo.codingtest.gamescoresservice.Application;
import com.oyo.codingtest.gamescoresservice.utils.TestUtils;

@RunWith(JUnitPlatform.class)
@SpringBootTest(classes = Application.class)
@TestMethodOrder(OrderAnnotation.class)
public class GameScoresControllerIntegrationTest {
	
	@Autowired
	private WebApplicationContext context;
	
	@Spy
	private HttpHeaders headers;
	
	private MockMvc mockMvc;
	
	@BeforeEach
	public void init() throws IOException, URISyntaxException {

		MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.webAppContextSetup(context).build();
		headers.add("content_type", MediaType.APPLICATION_JSON_VALUE);
	}

	@ParameterizedTest
	@MethodSource("getScoreTblObjParams")
	@Order(1)
	public void testSaveScoreOK(String scoreTbl) throws Exception {
		RequestBuilder requestBuilder = MockMvcRequestBuilders.post("/createScore")
										.headers(headers)
										.contentType(MediaType.APPLICATION_JSON_VALUE)
										.accept(MediaType.APPLICATION_JSON)
										.content(scoreTbl);
		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
		MockHttpServletResponse returnedResponse = result.getResponse();
		assertEquals(HttpStatus.OK.value(), returnedResponse.getStatus());
		
	}
	
	@Test
	@Order(2)
	public void testSaveScoreUQFail() throws Exception {
		RequestBuilder requestBuilder = MockMvcRequestBuilders.post("/createScore")
										.headers(headers)
										.contentType(MediaType.APPLICATION_JSON_VALUE)
										.accept(MediaType.APPLICATION_JSON)
										.content(TestUtils.getScoreTblStrObj());

		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
		MockHttpServletResponse returnedResponse = result.getResponse();
		assertEquals(HttpStatus.BAD_REQUEST.value(), returnedResponse.getStatus());
		
	}
	
	@Test
	@Order(3)
	public void testSaveScoreNegativeScoreFail() throws Exception {
		RequestBuilder requestBuilder = MockMvcRequestBuilders.post("/createScore")
										.headers(headers)
										.contentType(MediaType.APPLICATION_JSON_VALUE)
										.accept(MediaType.APPLICATION_JSON)
										.content(TestUtils.getScoreTblStrObjNegativeScore());

		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
		MockHttpServletResponse returnedResponse = result.getResponse();
		assertEquals(HttpStatus.INTERNAL_SERVER_ERROR.value(), returnedResponse.getStatus());
		
	}

	@Test
	@Order(4)
	public void testGetScoreByPlayerOK() throws Exception {
		RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/getScore")
				.headers(headers)
				.contentType(MediaType.APPLICATION_JSON_VALUE)
				.accept(MediaType.APPLICATION_JSON)
				.param("id", "1");

		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
		MockHttpServletResponse returnedResponse = result.getResponse();
		assertEquals(HttpStatus.OK.value(), returnedResponse.getStatus());
		assertNotNull(JsonPath.read(returnedResponse.getContentAsString(), "$.payload[0]"));	
		assertEquals(Integer.valueOf(1),JsonPath.read(returnedResponse.getContentAsString(), "$.payload[0].id"));		

	}
	
	@Test
	@Order(5)
	public void testGetScoresListWithFiltersOK() throws Exception {
		
		RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/getScoresList")
				.headers(headers)
				.contentType(MediaType.APPLICATION_JSON_VALUE)
				.accept(MediaType.APPLICATION_JSON)
				.param("startDate", "2020-11-30 00:00:00")
				.param("endDate", "2021-01-01 00:00:00")
				.param("players", "a")
				.param("size", "2")
				.param("page", "0");

		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
		MockHttpServletResponse returnedResponse = result.getResponse();
		assertEquals(HttpStatus.OK.value(), returnedResponse.getStatus());
		assertEquals(Integer.valueOf(2),JsonPath.read(returnedResponse.getContentAsString(), "$.payload.length()"));	

	}
	
	@Test
	@Order(6)
	public void testGetScoresListNoFiltersOK() throws Exception {
		
		RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/getScoresList")
				.headers(headers)
				.contentType(MediaType.APPLICATION_JSON_VALUE)
				.accept(MediaType.APPLICATION_JSON);

		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
		MockHttpServletResponse returnedResponse = result.getResponse();
		assertEquals(HttpStatus.OK.value(), returnedResponse.getStatus());
		assertEquals(Integer.valueOf(3),JsonPath.read(returnedResponse.getContentAsString(), "$.payload.length()"));	

	}
	
	@Test
	@Order(7)
	public void testGetPlayerHistoryOK() throws Exception {
		
		RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/getPlayerHistory")
				.headers(headers)
				.contentType(MediaType.APPLICATION_JSON_VALUE)
				.accept(MediaType.APPLICATION_JSON)
				.param("player", "a");

		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
		MockHttpServletResponse returnedResponse = result.getResponse();
		assertEquals(HttpStatus.OK.value(), returnedResponse.getStatus());
		assertEquals(Integer.valueOf(96), JsonPath.read(returnedResponse.getContentAsString(), "$.payload.averageScore"));
		assertEquals(Integer.valueOf(100), JsonPath.read(returnedResponse.getContentAsString(), "$.payload.topTimeScore.score"));
		assertEquals(Integer.valueOf(90), JsonPath.read(returnedResponse.getContentAsString(), "$.payload.leastTimeScore.score"));
		assertEquals(Integer.valueOf(100), JsonPath.read(returnedResponse.getContentAsString(), "$.payload.allScores[0].score"));
	}
	
	@Test
	@Order(8)
	public void testDeletePlayerOK() throws Exception {
		
		RequestBuilder requestBuilder = MockMvcRequestBuilders.delete("/deleteScore")
				.headers(headers)
				.contentType(MediaType.APPLICATION_JSON_VALUE)
				.accept(MediaType.APPLICATION_JSON)
				.param("id", "1");

		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
		MockHttpServletResponse returnedResponse = result.getResponse();
		assertEquals(HttpStatus.OK.value(), returnedResponse.getStatus());
		
	}
	
	@Test
	@Order(9)
	public void testDeleteUnavailablePlayer() throws Exception {
		
		RequestBuilder requestBuilder = MockMvcRequestBuilders.delete("/deleteScore")
				.headers(headers)
				.contentType(MediaType.APPLICATION_JSON_VALUE)
				.accept(MediaType.APPLICATION_JSON)
				.param("id", "10");

		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
		MockHttpServletResponse returnedResponse = result.getResponse();
		assertEquals(HttpStatus.INTERNAL_SERVER_ERROR.value(), returnedResponse.getStatus());
		
	}
	
	
	private static Stream<String> getScoreTblObjParams() {
		
		Stream<String> scoreTbls = Stream.of("{ \"player\": \"a\", \"score\": 100, \"time\": \"2020-12-31 00:00:00\"}",
				"{ \"player\": \"a\", \"score\": 100, \"time\": \"2020-12-31 01:00:00\"}",
				"{ \"player\": \"a\", \"score\": 90, \"time\": \"2020-12-31 02:00:00\"}");
		
		return scoreTbls;
	}
	
}
