package com.oyo.codingtest.gamescoresservice.utils;

import java.time.LocalDateTime;
import java.time.Month;
import java.time.format.DateTimeFormatter;
import java.util.Collections;

import com.oyo.codingtest.gamescoresservice.entity.ScoreTbl;
import com.oyo.codingtest.gamescoresservice.model.PlayerHistory;
import com.oyo.codingtest.gamescoresservice.model.TimeScore;
import com.oyo.codingtest.gamescoresservice.util.Constants;

public class TestUtils {
	
	public static ScoreTbl getScoreTblObj() {
		return ScoreTbl.builder().player("a").score(100).time(LocalDateTime.of(2020, Month.DECEMBER, 31, 00, 00, 00)).build();
	}
	
	public static ScoreTbl getScoreTblObjWithId() {
		return ScoreTbl.builder().id(1).player("a").score(100).time(LocalDateTime.of(2020, Month.DECEMBER, 31, 00, 00, 00)).build();
	} 
	
	public static String getScoreTblStrObjNegativeScore() {
		return "{ \"player\": \"a\", \"score\": -10, \"time\": \"2020-12-31 03:00:00\"}";
	}
	
	public static String getScoreTblStrObj() {
		return "{ \"player\": \"a\", \"score\": 100, \"time\": \"2020-12-31 00:00:00\"}";
	}
	
	public static TimeScore getTimeScoreObj() {
		return TimeScore.builder().time(LocalDateTime.of(2020, Month.DECEMBER, 31, 00, 00, 00)).score(100).build();
	}
	
	public static PlayerHistory getPlayerHistoryObj() {
		return PlayerHistory.builder().averageScore(100)
									.topTimeScore(getTimeScoreObj())
									.leastTimeScore(getTimeScoreObj())
									.allScores(Collections.singletonList(getTimeScoreObj()))
				.build();
		
	}

}
