package com.oyo.codingtest.gamescoresservice.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Collections;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.platform.runner.JUnitPlatform;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

import com.oyo.codingtest.gamescoresservice.entity.ScoreTbl;
import com.oyo.codingtest.gamescoresservice.model.PlayerHistoryResponse;
import com.oyo.codingtest.gamescoresservice.model.ScoreResponse;
import com.oyo.codingtest.gamescoresservice.repository.GameScoresRespository;
import com.oyo.codingtest.gamescoresservice.utils.TestUtils;

@RunWith(JUnitPlatform.class)
public class GameScoresServiceTest {

	@InjectMocks
	private GameScoresService gameScoresService;

	@Mock
	private ScoreTbl scoreTbl;
		
	@Mock
	private GameScoresRespository gameScoresRespository;
	
	@Spy
	private HttpHeaders headers;

	@BeforeEach
	public void init() throws IOException, URISyntaxException {
		MockitoAnnotations.initMocks(this);
		headers.add("content_type", MediaType.APPLICATION_JSON_VALUE);
	}

	@Test
	public void testCreateScoreOK() throws Exception {
		when(gameScoresRespository.save(TestUtils.getScoreTblObj())).thenReturn(TestUtils.getScoreTblObjWithId());
		ResponseEntity<ScoreResponse> response = gameScoresService.save(TestUtils.getScoreTblObj());  
		assertEquals(HttpStatus.OK.value(), response.getStatusCodeValue());
	}
	
	@Test
	public void testGetScoreByIdOK() throws Exception {
		when(gameScoresRespository.findById(1)).thenReturn(Optional.of(TestUtils.getScoreTblObjWithId()));
		ResponseEntity<ScoreResponse> response = gameScoresService.getScoreById(1);
		assertNotNull(response.getBody().getPayload().get(0));
		assertEquals(HttpStatus.OK.value(), response.getStatusCodeValue());
	}
	
	
	@Test
	public void testDeleteScoreByIdOK() throws Exception {
		ResponseEntity<ScoreResponse> response = gameScoresService.getScoreById(1);  
		assertEquals(HttpStatus.OK.value(), response.getStatusCodeValue());
	}
	
	
	@Test
	public void testGetPlayerHistoryOK() throws Exception {
		String player = "A";
		when(gameScoresRespository.findAverageScoreByPlayer(player)).thenReturn(100);
		when(gameScoresRespository.findTopScoreByPlayer(player)).thenReturn(TestUtils.getTimeScoreObj());
		when(gameScoresRespository.findLeastScoreByPlayer(player)).thenReturn(TestUtils.getTimeScoreObj());
		when(gameScoresRespository.findAllByPlayer(player)).thenReturn(Collections.singletonList(TestUtils.getTimeScoreObj()));

		ResponseEntity<PlayerHistoryResponse> response = gameScoresService.getPlayerHistory(player);  
		assertEquals(HttpStatus.OK.value(), response.getStatusCodeValue());
		assertEquals(Integer.valueOf(100), response.getBody().getPayload().getAverageScore());
		assertEquals(Integer.valueOf(100), response.getBody().getPayload().getTopTimeScore().getScore());
		assertEquals(Integer.valueOf(100), response.getBody().getPayload().getLeastTimeScore().getScore());
		assertEquals(Integer.valueOf(100), response.getBody().getPayload().getAllScores().get(0).getScore());
		
	}

}
