package com.oyo.codingtest.gamescoresservice.service;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import javax.persistence.criteria.Predicate;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import com.oyo.codingtest.gamescoresservice.entity.ScoreTbl;
import com.oyo.codingtest.gamescoresservice.model.PlayerHistory;
import com.oyo.codingtest.gamescoresservice.model.PlayerHistoryResponse;
import com.oyo.codingtest.gamescoresservice.model.ScoreResponse;
import com.oyo.codingtest.gamescoresservice.repository.GameScoresRespository;
import com.oyo.codingtest.gamescoresservice.util.HttpUtil;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@Service
@AllArgsConstructor
@NoArgsConstructor
public class GameScoresService {
		
		@Autowired
		private GameScoresRespository scoresRepo; 
		
		public ResponseEntity<ScoreResponse> save(ScoreTbl scoreTbl) {
			scoresRepo.save(scoreTbl);
			return HttpUtil.buildResponseNoPayload();
		}
		
		public ResponseEntity<ScoreResponse> getScoreById(Integer id) {
			return HttpUtil.buildResponseGetScore(scoresRepo.findById(id));
		}
		
		public ResponseEntity<ScoreResponse> deleteScoreById(Integer id) {
			scoresRepo.deleteById(id);
			return HttpUtil.buildResponseNoPayload();
		}
		        
		public ResponseEntity<ScoreResponse> getScoresList(LocalDateTime startDate, LocalDateTime endDate, String players, Pageable pageable){
			
			List<ScoreTbl> scoresList = scoresRepo.findAll((Specification<ScoreTbl>) (root, cq, cb) -> {
				Predicate p = cb.conjunction();

				if (Objects.nonNull(startDate)) {
					p = cb.and(p, cb.greaterThan(root.get("time"), startDate));
				}

				if (Objects.nonNull(endDate)) {
					p = cb.and(p, cb.lessThan(root.get("time"), endDate));
				}

				if (!ObjectUtils.isEmpty(players)) {
					p = cb.and(p, root.get("player").in(Arrays.asList(players.toUpperCase().split(","))));
				}

				cq.orderBy(cb.asc(root.get("player")), cb.asc(root.get("id")));
				return p;
			}, pageable).getContent();

			return HttpUtil.buildResponseGetScoreList(scoresList);
			
		}
		
		public ResponseEntity<PlayerHistoryResponse> getPlayerHistory(String player){
			
			return HttpUtil.buildResponseGetHistoryScore(PlayerHistory.builder().averageScore(scoresRepo.findAverageScoreByPlayer(player))
														.topTimeScore(scoresRepo.findTopScoreByPlayer(player))
														.leastTimeScore(scoresRepo.findLeastScoreByPlayer(player))
														.allScores(scoresRepo.findAllByPlayer(player))
														.build());
		}

			
}
