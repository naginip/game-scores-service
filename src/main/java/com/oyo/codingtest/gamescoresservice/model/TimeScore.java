package com.oyo.codingtest.gamescoresservice.model;

import java.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.oyo.codingtest.gamescoresservice.util.Constants;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TimeScore {
	
	@JsonFormat(pattern = Constants.DATE_PATTERN)
	private LocalDateTime time;
	private Integer score;

}


