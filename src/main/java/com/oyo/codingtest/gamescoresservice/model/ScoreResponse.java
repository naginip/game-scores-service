package com.oyo.codingtest.gamescoresservice.model;



import java.util.List;

import com.oyo.codingtest.gamescoresservice.entity.ScoreTbl;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ScoreResponse {
	
	private String statusMessage;
	private List<ScoreTbl> payload;
		
}
