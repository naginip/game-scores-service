package com.oyo.codingtest.gamescoresservice.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class PlayerHistoryResponse {
	
	private String statusMessage;
	private PlayerHistory payload;
		
}
