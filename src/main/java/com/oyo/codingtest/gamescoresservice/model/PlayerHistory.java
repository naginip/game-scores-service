package com.oyo.codingtest.gamescoresservice.model;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class PlayerHistory {
	
	private Integer averageScore;
	private TimeScore topTimeScore;
	private TimeScore leastTimeScore;
	private List<TimeScore> allScores;

}
