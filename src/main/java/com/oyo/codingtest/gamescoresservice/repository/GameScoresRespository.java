package com.oyo.codingtest.gamescoresservice.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.oyo.codingtest.gamescoresservice.entity.ScoreTbl;
import com.oyo.codingtest.gamescoresservice.model.TimeScore;

public interface GameScoresRespository extends JpaRepository<ScoreTbl, Integer> , JpaSpecificationExecutor<ScoreTbl> {
	
	@Query("SELECT new com.oyo.codingtest.gamescoresservice.model.TimeScore(s.time, s.score) FROM scoretbl AS s WHERE upper(s.player)=:player order by s.id asc")
	List<TimeScore> findAllByPlayer(@Param("player") String player);
	
	@Query("select new com.oyo.codingtest.gamescoresservice.model.TimeScore(max(s.time), max(s.score)) from scoretbl s where s.score = (select max(s1.score) from scoretbl s1 where upper(s1.player)=:player)")
	TimeScore findTopScoreByPlayer(@Param("player") String player);
	
	@Query("select new com.oyo.codingtest.gamescoresservice.model.TimeScore(max(s.time), min(s.score)) from scoretbl s where s.score = (select min(s1.score) from scoretbl s1 where upper(s1.player)=:player)")
	TimeScore findLeastScoreByPlayer(@Param("player") String player);
	
	@Query(value="select avg(s.score) score from scoretbl s where upper(s.player)=:player", nativeQuery=true)
	Integer findAverageScoreByPlayer(@Param("player") String player);
			
}
