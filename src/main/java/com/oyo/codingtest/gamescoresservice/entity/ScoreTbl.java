package com.oyo.codingtest.gamescoresservice.entity;

import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.Min;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.oyo.codingtest.gamescoresservice.util.Constants;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "scoretbl")
@Table(uniqueConstraints={@UniqueConstraint(columnNames = {"player", "time"})}) 
@JsonIgnoreProperties(ignoreUnknown = true)
public class ScoreTbl {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	private String player;
	
	@Min(value = 1, message = "Score should be greater than 0")
	private Integer score;
	 
	@JsonFormat(pattern = Constants.DATE_PATTERN)
	private LocalDateTime time;
		
	@PrePersist
    public void prePersist() {
        player = player.toUpperCase();
    }
	
    
}
