package com.oyo.codingtest.gamescoresservice.controller;

import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import com.oyo.codingtest.gamescoresservice.model.ScoreResponse;
import com.oyo.codingtest.gamescoresservice.util.Constants;
import com.oyo.codingtest.gamescoresservice.util.HttpUtil;

@RestControllerAdvice
public class GameScoresControllerAdvice {
	
	
	@ExceptionHandler(Exception.class)
	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	public ResponseEntity<ScoreResponse> handleGenericException(Exception exception) {
        return HttpUtil.buildFailureResponse(exception.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
	}
	
	@ExceptionHandler(DataIntegrityViolationException.class)
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	public ResponseEntity<ScoreResponse> handleSQLException1(DataIntegrityViolationException exception) {
        return HttpUtil.buildFailureResponse(Constants.UQ_EXCEPTION + exception.getMessage(), HttpStatus.BAD_REQUEST);
	}
	
}
