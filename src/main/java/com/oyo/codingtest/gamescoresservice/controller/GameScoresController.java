package com.oyo.codingtest.gamescoresservice.controller;

import java.time.LocalDateTime;

import javax.validation.Valid;

import org.springframework.data.domain.Pageable;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.oyo.codingtest.gamescoresservice.entity.ScoreTbl;
import com.oyo.codingtest.gamescoresservice.model.PlayerHistoryResponse;
import com.oyo.codingtest.gamescoresservice.model.ScoreResponse;
import com.oyo.codingtest.gamescoresservice.service.GameScoresService;
import com.oyo.codingtest.gamescoresservice.util.Constants;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@RestController
@AllArgsConstructor
@Validated
@Slf4j
public class GameScoresController {
	
	private GameScoresService scoresService;
		
	@PostMapping("/createScore")
	public ResponseEntity<ScoreResponse> saveScore(@RequestBody @Valid ScoreTbl score){
    	log.debug("Enter saveScore Controller");
		return scoresService.save(score);
	}
	
	@GetMapping("/getScore")
	public ResponseEntity<ScoreResponse> getScoreByPlayer(@RequestParam Integer id){
    	log.debug("Enter getScoreByPlayer Controller");
		return scoresService.getScoreById(id);
	}

	@DeleteMapping("/deleteScore")
	public ResponseEntity<ScoreResponse> deleteScoreById(@RequestParam Integer id){
    	log.debug("Enter deleteScoreById Controller");
		return scoresService.deleteScoreById(id);
	}
	
	@GetMapping("/getScoresList")
	public ResponseEntity<ScoreResponse> getScoresList(@RequestParam(required = false) @DateTimeFormat(pattern=Constants.DATE_PATTERN) LocalDateTime startDate, 
											@RequestParam(required = false) @DateTimeFormat(pattern=Constants.DATE_PATTERN) LocalDateTime endDate,
											@RequestParam(required = false) String players,
											Pageable pageable){
		log.debug("Enter getScoresList Controller");
		return scoresService.getScoresList(startDate, endDate, players, pageable);
	}
	
	@GetMapping("/getPlayerHistory")
	public ResponseEntity<PlayerHistoryResponse> getPlayerHistory(@RequestParam String player){
		log.debug("Enter getPlayerHistory Controller");
		return scoresService.getPlayerHistory(player.toUpperCase());
	}
    
}
