package com.oyo.codingtest.gamescoresservice.util;

import lombok.experimental.UtilityClass;

@UtilityClass
public class Constants {

	public static final String DATE_PATTERN = "yyyy-MM-dd HH:mm:ss";
	public static final String SUCCESS = "REQUEST PROCESSED SUCCESSFULLY";
	public static final String FAILURE = "UNEXPECTED ERROR :: ";
	public static final String UQ_EXCEPTION = "Unique Constraint (player, time) violated :: ";
		
}
