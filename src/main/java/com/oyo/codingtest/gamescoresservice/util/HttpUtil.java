package com.oyo.codingtest.gamescoresservice.util;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.oyo.codingtest.gamescoresservice.entity.ScoreTbl;
import com.oyo.codingtest.gamescoresservice.model.PlayerHistory;
import com.oyo.codingtest.gamescoresservice.model.PlayerHistoryResponse;
import com.oyo.codingtest.gamescoresservice.model.ScoreResponse;

import lombok.experimental.UtilityClass;


@UtilityClass
public class HttpUtil {
	
	public static ResponseEntity<ScoreResponse> buildResponseNoPayload() {
		return new ResponseEntity<>(ScoreResponse.builder().statusMessage(Constants.SUCCESS).build(), HttpStatus.OK);
	}
	
	public static ResponseEntity<ScoreResponse> buildResponseGetScore(Optional<ScoreTbl> scoreResult) {
		List<ScoreTbl> payload = new ArrayList<>();
		if(scoreResult.isPresent()) {
			payload.add(scoreResult.get());
		}
		return new ResponseEntity<>(ScoreResponse.builder().statusMessage(Constants.SUCCESS).payload(payload).build(), HttpStatus.OK);

	}
	
	public static ResponseEntity<ScoreResponse> buildResponseGetScoreList(List<ScoreTbl> scoreList) {
		return new ResponseEntity<>(ScoreResponse.builder().statusMessage(Constants.SUCCESS).payload(scoreList).build(), HttpStatus.OK);
	}
	
	public static ResponseEntity<PlayerHistoryResponse> buildResponseGetHistoryScore(PlayerHistory historyScore) {
		return new ResponseEntity<>(PlayerHistoryResponse.builder().statusMessage(Constants.SUCCESS).payload(historyScore).build(), HttpStatus.OK);
	}
	
	public static ResponseEntity<ScoreResponse> buildFailureResponse(String exception, HttpStatus responseCode) {
		return new ResponseEntity<>(ScoreResponse.builder().statusMessage(Constants.FAILURE + exception).build(), responseCode);

	}
	
	
}
