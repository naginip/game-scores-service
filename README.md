# Score Manager API #

Spring Boot/JPA + Postgres API to manage Scores of a game that covers CRUD operations.  
Tech stack used: Java 8, Gradle 6.7.1, Spring Boot 2.2, PostgreSQL, JUnit 5, Postman

### API Features ###


1. Create Score:
Creates a Score of a Player that accepts a payload with Player, Score, Time and assigns a unique ID. 

2. Get Score:
Retrieve the score of a Player by passing ID. 

3. Get list of scores:
Retrieves the complete Score list of all players, when no filters are applied. 
Also retrieves the list with any combination of filters like player/s, before a time, after a time, between two times.  

4. Get players' history
Retrieves the history of a input player like Top Score/time, Least Score/time, Average score and all scores/time.

5. Delete Score
Can delete a score based on input ID.



### How to Build/Run API locally ###

1. Download and install PostgreSQL.  Create an account and Database.
2. Download and configure Gradle 6+ version
3. Ensure the IDE is setup with lombok. 
4. Import the cloned repo as a Gradle Project in Eclipse/STS.
5. Configure the postgres DB details in application.properties
6. Execute the gradle build command and it should be Successful.
7. Run as Spring Boot Application from IDE.
8. Can test the APIs with the below cURLs reference.



### cURL References ###


#### 1. Create Score
curl --location --request POST 'localhost:8080/createScore' \
--header 'Content-Type: application/json' \
--data-raw '{
"player" : "player A",
"score": 90,
"time":"2020-08-29 14:27:00"
}'

Note: 
Score should be positive number only
Date should be passed in '2020-12-31 00:00:00' format.

#### 2. Get a Player Score
curl --location --request GET 'localhost:8080/getScore?id=1'

Note:
The unique ID generated while creating a Score should be passed.  Can refer DB for ID or run the endpoint 3 without filters for ID.

#### 3. Get list of Scores
curl --location --request GET 'localhost:8080/getScoresList?startDate=2018-12-20%2014:27:00&endDate=2020-12-30%2014:27:00&players=player%20A&size=1&page=0' \
--header 'Content-Type: application/json'

Note:  All or None or any of the parameters can be passed.  No Params call returns complete list in DB. 

#### 4. Get Player History
curl --location --request GET 'localhost:8080/getPlayerHistory?player=player%20A'

#### 5. Delete a Score
curl --location --request DELETE 'localhost:8080/deleteScore?id=1' \
--header 'Content-Type: application/json' 

Note:
The unique ID generated while creating a Score should be passed.  Can refer DB for ID or run the endpoint 3 without filters for ID.


### Run Unit Tests ###

There are 2 Unit Test classes each for a Controller and Service classes.
Eclipse/STS Project Explorer >  Right click the Test class > Run as Junit tests.  All tests should be passed.

### Run Integration Tests ###

GameScoresControllerIntegrationTest.java is the Integration test for this API.
Eclipse/STS Project Explorer >  Right click the GameScoresControllerIntegrationTest.java > Run as Junit tests.  All tests should be passed.
Please change the value of spring.jpa.hibernate.ddl-auto in application.properties from create-drop to none, to see results in DB after running Integration test.


